package sv.com.aloja;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class AlojaEurekaApp {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(AlojaEurekaApp.class, args);
	}
}
